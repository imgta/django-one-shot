from .models import TodoList, TodoItem
from django.forms import ModelForm


# TO-DO LIST FORM #
class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


# TO-DO TASKS/ITEMS FORM #
class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
