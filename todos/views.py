from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm


# VIEW ALL TO-DO LISTS #
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/todo_list.html", context)


# DETAILED TO-DO LIST VIEW #
def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/todo_list_detail.html", context)


# CREATE NEW TO-DO LIST #
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id)
    form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/todo_list_create.html", context)


# EDIT TO-DO LIST #
def todo_list_update(request, id):
    updated_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=updated_list)
        if form.is_valid():
            updated_list = form.save()
            return redirect("todo_list_detail", id=id)
    form = TodoListForm()
    context = {
        "todo_list_object": updated_list,
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


# DELETE TO-DO LIST #
def todo_list_delete(request, id):
    delete_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/todo_list_delete.html")


# ADD TASK ITEM TO TO-DO LIST #
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST or None)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_create.html", context)


# EDIT TASK ITEM #
def todo_item_update(request, id):
    updated_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=updated_item)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.id)
    form = TodoItemForm(instance=updated_item)
    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_update.html", context)


# def todo_item_update(request, id):
#     todo_item = get_object_or_404(TodoItem, id=id)
#     if request.method == "POST":
#         form = TodoItemForm(request.POST, instance=todo_item)
#         if form.is_valid():
#             form.save()
#             return redirect("todo_list_detail", id=todo_item.todo_list.id)
#     else:
#         form = TodoItemForm(instance=todo_item)
#     todo_lists = TodoList.objects.all()
#     context = {
#         "form": form,
#         "todo_item": todo_item,
#         "todo_lists": todo_lists,
#         "selected_list": todo_item.todo_list.id,
#     }
#     return render(request, "todos/todo_item_update.html", context)
